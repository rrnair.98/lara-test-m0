<?php

namespace Tests\FactoryHelpers;

use App\Models\User;
use Database\Factories\UserFactory;

class TokenFactoryHelper
{
    private User $user;

    private string $token;

    /**
     * @param User $user
     * @param string $token
     */
    private function __construct(User $user)
    {
        $this->user = $user;
        $this->token = $this->createToken();
    }

    private function createToken(): string {
        return $this->user->createToken()->token;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }


    public static function newTokenFactoryHelper(): TokenFactoryHelper {
        return self::newTokenFactoryHelperWithUser(UserFactory::new()->create());
    }

    public static function newTokenFactoryHelperWithUser(User $user): TokenFactoryHelper {
        return new TokenFactoryHelper($user);
    }

}
