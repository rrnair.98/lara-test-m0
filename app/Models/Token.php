<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'expires_at',
        'token'
    ];

    public const DEFAULT_EXPIRY_IN_MINUTES = 60;
}
