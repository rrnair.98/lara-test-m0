<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Creates a token instance for the user. Adds  Token::DEFAULT_EXPIRY_IN_MINUTES to now to set
     * the expiry of the token
     * @return Token
     */
    public function createToken(): Token
    {
        return Token::create([
            'user_id' => $this->id,
            'token' => sha1($this->name . $this->email . now()),
            'expires_at' => now()->addMinutes(Token::DEFAULT_EXPIRY_IN_MINUTES)
        ]);
    }

    public function tokens(): HasMany
    {
        return $this->hasMany(Token::class, 'user_id');
    }
}
