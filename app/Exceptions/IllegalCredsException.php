<?php

namespace App\Exceptions;

class IllegalCredsException extends \Exception
{
    public function __construct(string $email)
    {
        parent::__construct(sprintf(self::MESSAGE_FMT, $email));
    }

    private const MESSAGE_FMT = "wrong email and password received for email: %s";
}
