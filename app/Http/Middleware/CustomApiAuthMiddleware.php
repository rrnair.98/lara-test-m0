<?php

namespace App\Http\Middleware;

use App\Models\Token;
use App\Models\User;
use App\Services\ResponseHelper;
use Closure;

use Illuminate\Http\Request;
use Illuminate\Routing\Controllers\Middleware;
use Symfony\Component\HttpFoundation\Response;


class CustomApiAuthMiddleware
{
    public function handle(Request $request, Closure $next): Response
    {
        $currentToken = $request->header('Authorization');
        $pat = Token::where('token', $currentToken)->where('expires_at', '>', now())->first();
        if (is_null($pat)) {
            return ResponseHelper::tokenExpired();
        }
        $user = User::findOrFail($pat->user_id);
        auth()->login($user);
        $request->merge(['token' => $pat]);
        return $next($request);
    }
}
