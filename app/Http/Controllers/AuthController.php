<?php

namespace App\Http\Controllers;

use App\Services\ResponseHelper;
use App\Services\TokenService;
use App\Services\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private TokenService $tokenService;

    /**
     * @param TokenService $tokenService
     */
    public function __construct(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }


    private const LOGIN_RULES = [
        'email' => 'required|email',
        'password' => 'required'
    ];

    public function login(Request $request): JsonResponse
    {
        Validator::validateOrThrow(self::LOGIN_RULES, $request->all());
        return ResponseHelper::created([
            'token' => $this->tokenService->genToken($request->get('email'), $request->get('password')),
        ]);

    }


    public function logout(Request $request): JsonResponse {
        $request['token']->delete();
        return ResponseHelper::success();
    }
}
