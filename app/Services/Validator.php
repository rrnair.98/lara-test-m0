<?php
namespace App\Services;

use Illuminate\Validation\ValidationException;

class Validator {
    public static function validateOrThrow(array $validationRules, array $data): array {
        $validator = \Illuminate\Support\Facades\Validator::make($data, $validationRules);
        if ($validator->fails()) {
            throw ValidationException::withMessages($validator->getMessageBag()->toArray());
        }
        return $validator->validated();
    }
}
