<?php

namespace App\Services;

use App\Exceptions\IllegalCredsException;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class TokenService
{
    /**
     * @param string $email
     * @param string $password
     * @return string
     *
     * @throws IllegalCredsException When the creds are invalid
     * @throws ModelNotFoundException When the user cant be queried with the given email
     */
    public function genToken(string $email, string $password): string
    {
        $user = User::where('email', $email)->firstOrFail();
        Log::info("checking creds for email: $email, password: $password");
        if (!Hash::check($password, $user->password)) {
            throw new IllegalCredsException($email);
        }
        Log::info("creating token for email: $email");
        return $user->createToken()->token;
    }
}
